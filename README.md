Sejda-io (http://www.sejda.org)
=====
![Build Status](https://github.com/torakiki/sejda-io/actions/workflows/build.yml/badge.svg)
[![License](http://img.shields.io/badge/license-APLv2-blue.svg)](http://www.apache.org/licenses/LICENSE-2.0.html)

A layer of Input/Output classes built on top of java io and nio.

